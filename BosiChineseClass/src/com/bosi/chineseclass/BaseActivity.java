package com.bosi.chineseclass;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Toast;

import com.bosi.chineseclass.components.LoadingDialog;
import com.bosi.chineseclass.components.MyLoadingProgressBar;
import com.bosi.chineseclass.components.PhoneLoginTimeOutDialog;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.http.HttpHandler;
import com.umeng.analytics.MobclickAgent;

public class BaseActivity extends FragmentActivity {

	protected BitmapUtils mBitmapUtils;

	protected Activity mContext;

	public LoadingDialog mLoadingDialog;
	
	private final Object mSyObject = new Object();

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		this.mContext = this;
		ViewUtils.inject(this);
		BSApplication.getInstance().mActivityStack.push(this);
		mLoadingDialog = new LoadingDialog(this);
	}

	
	public String getResourceFromId(int id) {
		return getResources().getString(id);
	}

	public void showLoadingDialog() {
		synchronized (mSyObject) {
			mLoadingDialog.show();
		}
	}

	public void updateProgress(int progress, int max) {
		if (mLoadingDialog.mDialog==null||!mLoadingDialog.mDialog.isShowing()) {
			showLoadingDialog();
		}
		if(progress == max){
			dismissProgress();
		}
		mLoadingDialog.updateProgress(progress, max);
	}

	public void dismissProgress() {
		mLoadingDialog.dismiss();
	}

	public String getStringFormat(String format, String... args) {
		return format.format(format, args);
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
		//如果是手机号登陆 检查当前时间和登陆时间 超过限制时间提示试用时间已经到了
		//如果还没有登录的话 不检查
		checkPhoneLoginTimeDistance();
	}
	
	
	protected void checkPhoneLoginTimeDistance(){
		if(BSApplication.getInstance().mCurrentLoginRole ==BSApplication.ROLE_PHONELOGIN){
			long mCurrentTime = System.currentTimeMillis();
			//程序最后试用期限
			long mTimeminPhoneUserLoginTime = BSApplication.getInstance().mTimeminPhoneUserLoginTime;
			//距离最后试用期限的时间
			long mLoginTime = mCurrentTime - mTimeminPhoneUserLoginTime;
			
			if(mTimeminPhoneUserLoginTime>0&&mLoginTime>=0){
				showPhoneLoginTimeOutDialog();
			}
		}
	}
	
	protected void showPhoneLoginTimeOutDialog(){
		PhoneLoginTimeOutDialog mDialog = new PhoneLoginTimeOutDialog(this);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		BSApplication.getInstance().mActivityStack.remove(this);
		System.gc();
	}

	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	HttpHandler<String> mHttpHandler;

	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}

	public void showToastLong(String text) {
		Toast.makeText(this, text, Toast.LENGTH_LONG).show();
	}

	public void showToastShort(String text) {
		Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
	}

	public void playYoYo(View mView) {
		YoYo.with(Techniques.Shake).duration(700).playOn(mView);
		
	}

	//通过开一个线程去快速的实现一个异步的任务
    public void AsyTaskBaseThread(final Runnable mTask,final Runnable mResult){
			new Thread(){
					public void run(){
						mTask.run();
						runOnUiThread(mResult);
					}
			}.start();
		}
    
    MyLoadingProgressBar myLoadingProgressBar = new MyLoadingProgressBar(this);;
	public void showProgresssDialog() {
		showProgresssDialogWithHint(null);
	}
	public void showProgresssDialogWithHint(String hint) {
		if(myLoadingProgressBar==null)
			myLoadingProgressBar = new MyLoadingProgressBar(this);
			if(myLoadingProgressBar.isProgressShowing())myLoadingProgressBar.dialogDismiss();
			myLoadingProgressBar.showWithHint(hint);
    }

	public void dismissProgressDialog() {
		if(myLoadingProgressBar!=null &&myLoadingProgressBar.isProgressShowing())
		myLoadingProgressBar.dialogDismiss();
	}

}
