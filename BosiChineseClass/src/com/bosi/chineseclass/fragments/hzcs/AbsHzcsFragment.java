package com.bosi.chineseclass.fragments.hzcs;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.impl.client.DefaultHttpClient;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.webkit.WebSettings.ZoomDensity;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bosi.chineseclass.BaseFragment;
import com.bosi.chineseclass.R;
import com.bosi.chineseclass.XutilImageLoader;
import com.bosi.chineseclass.han.components.HeadLayoutComponents;
import com.bosi.chineseclass.utils.NetStateUtil;
import com.lidroid.xutils.bitmap.BitmapDisplayConfig;
import com.lidroid.xutils.bitmap.callback.BitmapLoadCallBack;
import com.lidroid.xutils.bitmap.callback.BitmapLoadFrom;

//汉字常识的基础功能
public abstract class AbsHzcsFragment extends BaseFragment  {


	LinearLayout mLayoutMenu;

	String mCurrentData[];
	String mCurrentDataHtmlData[];
	int currentPosition = -1;

	View mViewHead;

	protected HeadLayoutComponents mHeadActionBar;

	View mViewBody;

	TextView mTvDitalTitle;

	LinearLayout mLayoutWebViewBody;

	WebView mWebView;

	ProgressBar mProgressBar;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (savedInstanceState != null)
			return;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	protected View getBasedView() {
		return View.inflate(mActivity, R.layout.layout_hzcs_base, null);
	}

	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void afterViewInject() {
		mLayoutMenu = (LinearLayout) mBaseView
				.findViewById(R.id.ll_hzcs_leftmenu);



		mWebView = (WebView) mBaseView.findViewById(R.id.wv_hzcs_dital);
        
        
		mProgressBar = (ProgressBar) mBaseView.findViewById(R.id.pb_forwebview);
		mLayoutWebViewBody = (LinearLayout) mBaseView
				.findViewById(R.id.ll_hzcs_dital);

		mTvDitalTitle = (TextView) mBaseView
				.findViewById(R.id.tv_hzcsdital_title);
		mViewHead = mBaseView.findViewById(R.id.headactionbar);
		mViewBody = mBaseView.findViewById(R.id.rl_hzcs_body);
		displayBgView();
		mHeadActionBar = new HeadLayoutComponents(mActivity, mViewHead);
		initMenu();
		initWebView();
	}

	@SuppressLint("SetJavaScriptEnabled")
	private void initWebView() {
		WebSettings webSettings = mWebView.getSettings();
		//webSettings.setUseWideViewPort(true); 自适应屏幕
		webSettings.setJavaScriptEnabled(true);
		webSettings.setSupportZoom(true); // 可以缩放
		
		webSettings.setBuiltInZoomControls(true); // 显示放大缩小 controler
//		webSettings.setDefaultZoom(ZoomDensity.CLOSE);// 默认缩放模式
//		webSettings.setUseWideViewPort(true);
//		webSettings.setLoadWithOverviewMode(true); 
		mWebView.setWebViewClient(new WebViewClient() {

			boolean isError = false;

			public void onReceivedError(WebView view, int errorCode,
					String description, String failingUrl) {
				view.stopLoading();
				view.clearFormData();
				view.clearHistory();
//				if (isError) {
//					displayCurrentPic();
//				}
			}

			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				view.loadUrl(url);
				return true;
			}
		});
		

		mWebView.setWebChromeClient(new WebChromeClient() {

			@Override
			public void onProgressChanged(WebView view, int newProgress) {
				super.onProgressChanged(view, newProgress);
				if (newProgress == 100) {
					loadVideo();
					mProgressBar.setVisibility(View.GONE);
					return;
				}
				mProgressBar.setVisibility(View.VISIBLE);
				mProgressBar.setProgress(newProgress);
			}

		});

	}

	public abstract void initMenu();


	/**
	 * 如果网络状态不通直接返回 -1
	 * 
	 * */
	private int getRespStatus(String url) {

		int status = -1;

		if (!NetStateUtil.isNetWorkAlive(mActivity)) {
			return status;
		}

		try {

			HttpHead head = new HttpHead(url);

			HttpClient client = new DefaultHttpClient();

			HttpResponse resp = client.execute(head);

			status = resp.getStatusLine().getStatusCode();

		} catch (IOException e) {
		}

		return status;

	}

	int isExistHtmlFile = -1;

	protected synchronized void updateDitalPg() {
		
		//TODO 不使用图片完全使用网页
//		if (mCurrentData == null)
//			return;
//		if (currentPosition == -1 || currentPosition >= mCurrentData.length) {
//			currentPosition = 0;
//
//		}
//		// 换成加载网页 否则
//		mIvDital.setVisibility(View.GONE);
//		mLayoutWebViewBody.setVisibility(View.VISIBLE);
//		mWebView.clearHistory();
//
//		// 首先异步检查当前文件是否存在如果不存在直接加载图片
//		final String mCurrentJpgPath = mCurrentData[currentPosition];
//		final String mUrlPath = mCurrentJpgPath.substring(0,
//				mCurrentJpgPath.lastIndexOf("."))
//				+ ".html";
//
//		mActivity.showProgresssDialogWithHint("加载中...");
//		isExistHtmlFile = -1;
//		mActivity.AsyTaskBaseThread(new Runnable() {
//
//			@Override
//			public void run() {
//				isExistHtmlFile = getRespStatus(mUrlPath);
//			}
//		}, new Runnable() {
//
//			@Override
//			public void run() {
///*				if (isExistHtmlFile != -1 && isExistHtmlFile == 200) {
//					mWebView.loadUrl(mUrlPath);
//				} else {
//					displayCurrentPic();
//				}*/
//				
//				mWebView.loadUrl("http://www.yuwen100.cn/yuwen100/hzzy/Android/zaozifangfa/xx/index.html");
//				
//				mActivity.dismissProgressDialog();
//			}
//		});
		mLayoutWebViewBody.setVisibility(View.VISIBLE);
		mWebView.loadUrl(mCurrentData[0]);
		mActivity.dismissProgressDialog();

	}
	
	private void loadVideo()
	{
		String js="javascript: var v=document.getElementsByTagName('video')[0]; "+"v.play(); ";
		mWebView.loadUrl(js);
	}


	protected abstract void downLoadImageOverAction();


	protected void displayBgView() {
		mViewBody.setBackgroundResource(R.drawable.hzqy_ditalbg);
	}

	/*---------------添加下载模块----------------*/

	int loadedData = -1;

	public void downloadimgs() {
		
		//todo 删除之前的下载逻辑
//		if (mCurrentData.length > 1) {
//			mBtLeft.setVisibility(View.GONE);
//			mBtRight.setVisibility(View.VISIBLE);
//		} else {
//			mBtLeft.setVisibility(View.GONE);
//			mBtRight.setVisibility(View.GONE);
//		}
//		loadedData = -1;
//		updateProgress();
//		for (int i = 0; i < mCurrentData.length; i++) {
//			mImageLoader.getBitmapFactory().display(mIvDital, mCurrentData[i],
//					new BitmapLoadCallBack<View>() {
//
//						@Override
//						public void onLoadCompleted(View container, String uri,
//								Bitmap bitmap, BitmapDisplayConfig config,
//								BitmapLoadFrom from) {
//							updateProgress();
//						}
//
//						@Override
//						public void onLoadFailed(View container, String uri,
//								Drawable drawable) {
//							updateProgress();
//						}
//
//						@Override
//						public void onLoading(View container, String uri,
//								BitmapDisplayConfig config, long total,
//								long current) {
//							super.onLoading(container, uri, config, total,
//									current);
//						}
//
//					});
//		}
		
		downLoadImageOverAction();
	}

	private void updateProgress() {
		loadedData++;
		mActivity.updateProgress(loadedData, mCurrentData.length);
		if (loadedData == mCurrentData.length) {
			mActivity.dismissProgress();
			downLoadImageOverAction();
			loadedData = 0;

		}
	}

}
